{-
    chikun :: 2014
    Testing Haskell
-}

-- Doubles two values and returns the sum
doubleUs x y = x * 2 + y * 2

-- Define a new operator
(//) a b = a `div` b
